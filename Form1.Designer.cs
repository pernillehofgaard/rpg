﻿namespace RPGGUI
{

    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.EnteredName = new System.Windows.Forms.TextBox();
            this.cbCharacters = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblChooseClass = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnFetchCharacters = new System.Windows.Forms.Button();
            this.lbCharacter = new System.Windows.Forms.ListBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(15, 56);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(98, 20);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Enter Name:";
            // 
            // EnteredName
            // 
            this.EnteredName.Location = new System.Drawing.Point(127, 53);
            this.EnteredName.Name = "EnteredName";
            this.EnteredName.Size = new System.Drawing.Size(194, 26);
            this.EnteredName.TabIndex = 1;
            // 
            // cbCharacters
            // 
            this.cbCharacters.FormattingEnabled = true;
            this.cbCharacters.Items.AddRange(new object[] {
            "characters"});
            this.cbCharacters.Location = new System.Drawing.Point(127, 6);
            this.cbCharacters.Name = "cbCharacters";
            this.cbCharacters.Size = new System.Drawing.Size(194, 28);
            this.cbCharacters.TabIndex = 2;
            this.cbCharacters.SelectedIndexChanged += new System.EventHandler(this.cbCharacter_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 186);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 20);
            this.label1.TabIndex = 6;
            // 
            // lblChooseClass
            // 
            this.lblChooseClass.AutoSize = true;
            this.lblChooseClass.Location = new System.Drawing.Point(13, 9);
            this.lblChooseClass.Name = "lblChooseClass";
            this.lblChooseClass.Size = new System.Drawing.Size(108, 20);
            this.lblChooseClass.TabIndex = 8;
            this.lblChooseClass.Text = "Choose class:";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(127, 102);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(194, 31);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Create";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnFetchCharacters
            // 
            this.btnFetchCharacters.Location = new System.Drawing.Point(538, 228);
            this.btnFetchCharacters.Name = "btnFetchCharacters";
            this.btnFetchCharacters.Size = new System.Drawing.Size(194, 33);
            this.btnFetchCharacters.TabIndex = 10;
            this.btnFetchCharacters.Text = "Update List";
            this.btnFetchCharacters.UseVisualStyleBackColor = true;
            this.btnFetchCharacters.Click += new System.EventHandler(this.btnFetchCharacter_Click);
            // 
            // lbCharacter
            // 
            this.lbCharacter.BackColor = System.Drawing.SystemColors.Window;
            this.lbCharacter.FormattingEnabled = true;
            this.lbCharacter.ItemHeight = 20;
            this.lbCharacter.Location = new System.Drawing.Point(350, 6);
            this.lbCharacter.Name = "lbCharacter";
            this.lbCharacter.Size = new System.Drawing.Size(613, 204);
            this.lbCharacter.TabIndex = 11;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(127, 228);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(194, 34);
            this.btnDelete.TabIndex = 12;
            this.btnDelete.Text = "Delete a Character";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(127, 284);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(194, 31);
            this.button1.TabIndex = 13;
            this.button1.Text = "Edit Character";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 433);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.lbCharacter);
            this.Controls.Add(this.btnFetchCharacters);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblChooseClass);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbCharacters);
            this.Controls.Add(this.EnteredName);
            this.Controls.Add(this.lblName);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox EnteredName;
        private System.Windows.Forms.ComboBox cbCharacters;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblChooseClass;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnFetchCharacters;
        private System.Windows.Forms.ListBox lbCharacter;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button button1;
    }
}

