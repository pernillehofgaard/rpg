﻿using System;

namespace RPGGUI
{
    partial class UpdateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUpdateName = new System.Windows.Forms.Button();
            this.lblNewName = new System.Windows.Forms.Label();
            this.tbEnteredNewName = new System.Windows.Forms.TextBox();
            this.lbCharacters = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbEnteredID = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnUpdateName
            // 
            this.btnUpdateName.Location = new System.Drawing.Point(12, 391);
            this.btnUpdateName.Name = "btnUpdateName";
            this.btnUpdateName.Size = new System.Drawing.Size(158, 35);
            this.btnUpdateName.TabIndex = 1;
            this.btnUpdateName.Text = "Save Changes";
            this.btnUpdateName.UseVisualStyleBackColor = true;
            this.btnUpdateName.Click += new System.EventHandler(this.btnUpdateName_Click);
            // 
            // lblNewName
            // 
            this.lblNewName.AutoSize = true;
            this.lblNewName.Location = new System.Drawing.Point(12, 358);
            this.lblNewName.Name = "lblNewName";
            this.lblNewName.Size = new System.Drawing.Size(133, 20);
            this.lblNewName.TabIndex = 2;
            this.lblNewName.Text = "Enter new name: ";
            // 
            // tbEnteredNewName
            // 
            this.tbEnteredNewName.Location = new System.Drawing.Point(151, 352);
            this.tbEnteredNewName.Name = "tbEnteredNewName";
            this.tbEnteredNewName.Size = new System.Drawing.Size(131, 26);
            this.tbEnteredNewName.TabIndex = 3;
            // 
            // lbCharacters
            // 
            this.lbCharacters.FormattingEnabled = true;
            this.lbCharacters.ItemHeight = 20;
            this.lbCharacters.Location = new System.Drawing.Point(12, 13);
            this.lbCharacters.Name = "lbCharacters";
            this.lbCharacters.Size = new System.Drawing.Size(784, 264);
            this.lbCharacters.TabIndex = 4;
            this.lbCharacters.SelectedIndexChanged += new System.EventHandler(this.lbCharacters_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 303);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "ID to character";
            // 
            // tbEnteredID
            // 
            this.tbEnteredID.Location = new System.Drawing.Point(151, 303);
            this.tbEnteredID.Name = "tbEnteredID";
            this.tbEnteredID.Size = new System.Drawing.Size(131, 26);
            this.tbEnteredID.TabIndex = 6;
            // 
            // UpdateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tbEnteredID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbCharacters);
            this.Controls.Add(this.tbEnteredNewName);
            this.Controls.Add(this.lblNewName);
            this.Controls.Add(this.btnUpdateName);
            this.Name = "UpdateForm";
            this.Text = "UpdateForm";
            this.Load += new System.EventHandler(this.UpdateForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void lbCharacters_SelectedIndexChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion
        private System.Windows.Forms.Button btnUpdateName;
        private System.Windows.Forms.Label lblNewName;
        private System.Windows.Forms.TextBox tbEnteredNewName;
        private System.Windows.Forms.ListBox lbCharacters;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbEnteredID;
    }
}