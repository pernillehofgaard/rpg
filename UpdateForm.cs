﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace RPGGUI
{
    public partial class UpdateForm : Form
    {
		SqlConnectionStringBuilder builder = null;
		public UpdateForm()
        {
            InitializeComponent();
        }

        private void UpdateForm_Load(object sender, EventArgs e)
        {
			//SQL Connection
			builder = new SqlConnectionStringBuilder();
			builder.DataSource = "PC7370\\SQLEXPRESS"; //generated dbName
			builder.InitialCatalog = "RPGCharacter";
			builder.IntegratedSecurity = true;

			//Adding characters to listbox
			try
			{
				string sql = "SELECT * FROM Character";

				using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
				{
					connection.Open();


					using (SqlCommand command = new SqlCommand(sql, connection))
					{
						using (SqlDataReader reader = command.ExecuteReader())
						{
							lbCharacters.Items.Add(reader.GetName(0) + "\t" + reader.GetName(1) + "\t" + reader.GetName(2) + "\t" + reader.GetName(3) + "\t" + reader.GetName(4) + "\t" + reader.GetName(5));
							
							while (reader.Read())
							{
								lbCharacters.Items.Add(reader.GetInt32(0) + "\t" + reader.GetString(1) + "\t" + reader.GetInt32(2) + "\t" + reader.GetInt32(3) + "\t" + reader.GetInt32(4) + "\t\t" + reader.GetString(5));
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void Update(string newName, int ID)
        {
            try
            {
				using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
				{
					connection.Open();
					string sql = "UPDATE Character SET Name = @newName WHERE ID = @ID";

					using (SqlCommand command = new SqlCommand(sql, connection))
					{
						command.Parameters.AddWithValue("@newName", newName);
						command.Parameters.AddWithValue("@ID", ID);
						command.ExecuteNonQuery();
						MessageBox.Show("Saved");
					}
				}
			}
			catch(Exception ex)
            {
				MessageBox.Show(ex.Message);
            }
			
        }

        private void btnUpdateName_Click(object sender, EventArgs e)
        {
            try
            {
				int id = Convert.ToInt32(tbEnteredID.Text);
				string newName = tbEnteredNewName.Text;
				Update(newName, id);
			}catch(Exception ex)
            {
				MessageBox.Show(ex.Message);
            }
			
        }
    }
}
