﻿using RPGFramework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace RPGGUI
{
    public partial class Form1 : Form
	{
		SqlConnectionStringBuilder builder = null;
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			Dictionary<int, string> characters = new Dictionary<int, string>();

			//Adding Classes to Combobox
			characters.Add(1, "Warrior");
			characters.Add(2, "Theif");
			characters.Add(3, "Wizard");

			cbCharacters.ValueMember = "Key";
			cbCharacters.DisplayMember = "Value";

			cbCharacters.DataSource = new BindingSource(characters, null);

			//SQL Connection
			builder = new SqlConnectionStringBuilder();
			builder.DataSource = "PC7370\\SQLEXPRESS"; //generated dbName
			builder.InitialCatalog = "RPGCharacter";
			builder.IntegratedSecurity = true;

			//Adding characters to listbox
			try
			{
				string sql = "SELECT * FROM Character";

				using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
				{
					connection.Open();


					using (SqlCommand command = new SqlCommand(sql, connection))
					{
						using (SqlDataReader reader = command.ExecuteReader())
						{
							lbCharacter.Items.Add(reader.GetName(0) + "\t" + reader.GetName(1) + "\t" + reader.GetName(2) + "\t" + reader.GetName(3) + "\t" + reader.GetName(4) + "\t" + reader.GetName(5));
							while (reader.Read())
							{
								lbCharacter.Items.Add(reader.GetInt32(0) + "\t" + reader.GetString(1) + "\t" + reader.GetInt32(2) + "\t" + reader.GetInt32(3) + "\t" + reader.GetInt32(4) + "\t\t" + reader.GetString(5));
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void cbCharacter_SelectedIndexChanged(object sender, EventArgs e)
		{
			int selectedIndex = cbCharacters.SelectedIndex;
			Object selectedItem = cbCharacters.SelectedItem;
		}

        private void btnSave_Click(object sender, EventArgs e)
        {
			int charId = cbCharacters.SelectedIndex;

            try
            {
                using(StreamWriter writer = new StreamWriter("Characters.txt"))
                {
					switch (charId)
					{
						case 0:
							Warrior warrior = new Warrior(120, 70, EnteredName.Text, 120);
							MessageBox.Show("Name: " + warrior.Name + "\nClass: Warrior\nHP: " + warrior.HP + "\nMana: " + warrior.Mana + "\nArmor rating: " + warrior.ArmorRating);
							writer.WriteLine("Class: Warrior" + ", name: " + warrior.Name + ", HP: " + warrior.HP + ", mana : " + warrior.Mana + ", armor rating: " + warrior.ArmorRating + "\n");
							InsertIntoDB("Warrior", warrior.HP, warrior.Mana, warrior.ArmorRating, warrior.Name);
							break;

						case 1:
							Theif theif = new Theif(80, 70, EnteredName.Text, 80);
							MessageBox.Show("Name: " + theif.Name + "\nClass: Theif\nHP: " + theif.HP + "\nMana: " + theif.Mana + "\nArmor rating: " + theif.ArmorRating);
							writer.WriteLine("Class: Theif" + ", name: " + theif.Name + ", HP: " + theif.HP + ", mana : " + theif.Mana + ", armor rating: " + theif.ArmorRating + "\n");
							InsertIntoDB("Theif", theif.HP, theif.Mana, theif.ArmorRating, theif.Name);
							break;

						case 2:
							Wizard wizard = new Wizard(60, 110, EnteredName.Text, 40);
							MessageBox.Show("Name: " + wizard.Name + "\nClass : Wizard\nHP: " + wizard.HP + "\nMana: " + wizard.Mana + "\nArmor rating: " + wizard.ArmorRating);
							writer.WriteLine("Class: Wizard" + ", name: " + wizard.Name + ", HP: " + wizard.HP + ", mana : " + wizard.Mana + ", armor rating: " + wizard.ArmorRating + "\n");
							InsertIntoDB("Wizard", wizard.HP, wizard.Mana, wizard.ArmorRating, wizard.Name);
							break;

						default:
							MessageBox.Show("ERROR");
							break;
					}
				}
				
			}
			catch(Exception ex)
            {
				MessageBox.Show(ex.Message);
            }
			
		}

		//INSERT INTO
		private void InsertIntoDB(string Class, int HP, int Mana, int ArmorRating, string Name) 
		{
			string sql = "INSERT INTO Character(Class, HP, Mana, ArmorRating, Name) VALUES (@characterClass, @hp, @mana, @armorRating, @name)";

            try
            {
				builder = new SqlConnectionStringBuilder();
				builder.DataSource = @"PC7370\SQLEXPRESS"; //generated dbName
				builder.InitialCatalog = "RPGCharacter";
				builder.IntegratedSecurity = true;

				using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
					connection.Open();

					using (SqlCommand command = new SqlCommand(sql, connection))
                    {
						command.Parameters.AddWithValue("@characterClass", Class);
						command.Parameters.AddWithValue("@hp", HP);
						command.Parameters.AddWithValue("@mana", Mana);
						command.Parameters.AddWithValue("@armorRating", ArmorRating); //typo in db
						command.Parameters.AddWithValue("@name", Name);

						command.ExecuteNonQuery();
                    }
                }
            }catch(Exception ex)
            {
				MessageBox.Show(ex.Message);
            }
		}


        private void btnFetchCharacter_Click(object sender, EventArgs e)
        {
			lbCharacter.Items.Clear();
			try
			{
				string sql = "SELECT * FROM Character";

				using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
				{
					connection.Open();


					using (SqlCommand command = new SqlCommand(sql, connection))
					{
						using (SqlDataReader reader = command.ExecuteReader())
						{
							lbCharacter.Items.Add(reader.GetName(0) + "\t" + reader.GetName(1) + "\t" + reader.GetName(2) + "\t" + reader.GetName(3) + "\t" + reader.GetName(4) + "\t" + reader.GetName(5));

							while (reader.Read())
							{
								lbCharacter.Items.Add(reader.GetInt32(0) + "\t" + reader.GetString(1) + "\t" + reader.GetInt32(2) + "\t" + reader.GetInt32(3) + "\t" + reader.GetInt32(4) + "\t\t" + reader.GetString(5));
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}

		}

        private void btnDelete_Click(object sender, EventArgs e)
        {
			//Delete Form
			Form2 form2 = new Form2();
			form2.Show();
		}

        private void button1_Click(object sender, EventArgs e)
        {
			UpdateForm updateFrom = new UpdateForm();
			updateFrom.Show();
        }
    }
}
