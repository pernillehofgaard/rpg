﻿using System;
using RPGFramework;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace RPGGUI
{
    public partial class Form2 : Form
    {
		SqlConnectionStringBuilder builder = null;

		public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
			//SQL Connection
			builder = new SqlConnectionStringBuilder();
			builder.DataSource = "PC7370\\SQLEXPRESS"; //generated dbName
			builder.InitialCatalog = "RPGCharacter";
			builder.IntegratedSecurity = true;

			//Adding characters to listbox
			try
			{
				string sql = "SELECT * FROM Character";

				using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
				{
					connection.Open();


					using (SqlCommand command = new SqlCommand(sql, connection))
					{
						using (SqlDataReader reader = command.ExecuteReader())
						{
							lbCharacters.Items.Add(reader.GetName(0) + "\t" + reader.GetName(1) + "\t" + reader.GetName(2) + "\t" + reader.GetName(3) + "\t" + reader.GetName(4) + "\t" + reader.GetName(5));
							while (reader.Read())
							{
								lbCharacters.Items.Add(reader.GetInt32(0) + "\t" + reader.GetString(1) + "\t" + reader.GetInt32(2) + "\t" + reader.GetInt32(3) + "\t" + reader.GetInt32(4) + "\t\t" + reader.GetString(5));
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		//Delete
		private void DeleteFromDB(int characterID)
        {
            try
            {
				using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
				{
					connection.Open();

					string sql = "DELETE FROM Character WHERE ID = @characterId";
					using (SqlCommand command = new SqlCommand(sql, connection))
					{
						command.Parameters.AddWithValue("@characterId", characterID);
						command.ExecuteNonQuery();
					}
				}
			}
			catch(Exception ex)
            {
				MessageBox.Show(ex.Message);
            }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
				int id = Convert.ToInt32(tbEnteredId.Text);

				DeleteFromDB(id);
				MessageBox.Show("Deleted index" + lbCharacters.SelectedIndex + " \nItem: " + lbCharacters.SelectedItem);
			}catch(Exception ex)
            {
				MessageBox.Show(ex.Message);
            }
			
		}

    }
}
